# blockmodels4inventories

a Collection of R Functions to Analyze Inventories Data

http://nicolas.verzelen.pages.mia.inra.fr/blockmodels4inventories/

Using a latent block model by encapsulating the **blockmodels** library in the **blockmodels4inventories** library to perform biclustering on survey data to characterize the crop diversity and diversity of seeds supply modes..

possible input data: Presence/absence and count data

* on crop diversity
* on crop uses
* on sources of seed supply

as well as . . . many covariates

# Installation

To install the **blockmodels4inventories** package, the easiest is to install it directly from GitLab. Open an R session and run the following commands:

```R
if (!require("remotes")) {
  install.packages("remotes")
}
remotes::install_gitlab("nicolas.verzelen/blockmodels4inventories", 
                        host = "forgemia.inra.fr",
                        build_vignettes=TRUE)

```

# Usage

Once the package is installed on your computer, it can be loaded into a R session:

```R
library(blockmodels4inventories)
help(package="blockmodels4inventories")
```

Main specifications:

* LBM as a model-based bi-clustering method
* Easily handles covariates

![incid](man/figures/incidence2.PNG)

![biclust](man/figures/classic2.PNG)

![count](man/figures/count.PNG)

# Citation

As a lot of time and effort were spent in creating the **blockmodels4inventories** and **blockmodels**  methods, please cite it when using it for data analysis:

Jean-Benoist Leger (2016). Blockmodels: A R-package for estimating in Latent Block Model and Stochastic Block Model, with various probability functions, with or without covariates.  	arXiv:1602.07587

Jean-Benoist Leger (2015). Blockmodels : Latent and Stochastic Block Model
Estimation by a 'V-EM' Algorithm.

You should also cite the **blockmodels4inventories** package:

```R
citation("blockmodels4inventories")
```

See also citation() for citing R itself.
